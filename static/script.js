$(document).ready(function() {
    const grid = $('.content .right-content .rows');
    const updateGrid = () => { 
        $.ajax({
            url:'http://95.179.241.170:3001/graphql',
            method: 'POST',
            contentType: "application/json",
            data: JSON.stringify({"query":"{\n  allPayCalcsList(first: 25, orderBy:TIME_ARR_DESC, filter: {timeDep: {isNull:true}}) {\n    userByUserId {\n      number\n    }\n    parkingTime\n    pay\n    payTime\n    freeTime\n  }\n}\n"}),
            success: (result) => {
                const rows = result.data.allPayCalcsList.map(row => {
                    const lostFreeTime = row.payTime > 0 ? 0 : row.freeTime - row.parkingTime;
                    return `<div class="row">
                    <div class="number"><i class="fas fa-car"></i> ${row.userByUserId.number}</div>
                    <div class="parking-time"><i class="far fa-clock"></i> ${row.parkingTime} мин</div>
                    <div class="free-time"><i class="far fa-clock"></i> ${lostFreeTime} мин</div>
                    <div class="pay-time"><i class="far fa-clock"></i> ${row.payTime} мин</div>
                    <div class="pay"><i class="fas fa-coins"></i> ${Math.floor(row.pay/100)}.${row.pay%100} <i class="fas fa-ruble-sign"></i></div>
                </div>`;
                });
                grid.html(rows);
            }
        });
    };

    const form = $('.content .left-form');
    form.find("#in").on('click', function () {
        $.ajax({
            url: 'http://95.179.241.170:3000/in',
            method: 'GET',
            data: form.find('form').serialize(),
            success: updateGrid(),
        });
    });

    form.find("#out").on('click', function () {
        $.ajax({
            url: 'http://95.179.241.170:3000/out',
            method: 'GET',
            data: form.find('form').serialize(),
            success: updateGrid(),
        });
    });

    updateGrid();
});