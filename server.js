const express = require("express");

const app = express();

app.use(express.static(`${__dirname}/static`));
console.log('OK');
app.listen(process.env.PORT || 3010);